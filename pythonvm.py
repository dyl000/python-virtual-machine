import math

stack = []
variables = {}

def _ischarint(_char):
    return ( ord(_char) >= ord('0') and ord(_char) <= ord('9') or _char == "." )

def _isvalidvarchar(_char):
    return ( ord(_char) >= ord('A') and ord(_char) <= ord('Z') or ord(_char) >= ord('0') and ord(_char) <= ord('9') )

def _push(dat):
   stack.insert(0,dat)

def _exec(inst):
   if inst == "ADD":
       _push( float(stack[1]) + float(stack[0]) )
   elif inst == "DIV":
       _push( float(stack[1]) / float(stack[0]) )
   elif inst == "MUL":
       _push( float(stack[1]) * float(stack[0]) )
   elif inst == "SUB":
       _push( float(stack[1]) - float(stack[0]) )
   elif inst == "APP":
       push ( stack[1] + stack[0] )
   elif inst == "POP":
       del stack[0]
   elif inst == "LOAD":       
       _push( variables[stack[0]] )
       del stack[1]
   elif inst[0:4] == "CALL":
       if inst[5:len(inst)] == "PRINT":
           print(stack[0])
       if inst[5:len(inst)] == "INPUT":
           _push(input("Input > "))
       if inst[5:len(inst)] == "SQRT":
           _push(math.sqrt(float(stack[0])))
   elif inst[0:4] == "PUSH":
       _push( inst[5:len(inst)] )
   elif inst[0:4] == "SAVE":
       variables[ inst[5:len(inst)] ] = stack[0]

def sourcelexer(sourcecode):
    srcarr = list( sourcecode )
    tokenarray = []
    i = 0
    while i < len(srcarr): 
        if srcarr[i:i+2] == ["I","F"]:
            tokenarray.append(["LOGIC", "IF", ""])
            i += 1
        elif srcarr[i] == "+":
            tokenarray.append(["OPER", "ADD", ""])
        elif srcarr[i] == "/":
            tokenarray.append(["OPER", "DIV", ""])
        elif srcarr[i] == "*":
            tokenarray.append(["OPER", "MUL", ""])
        elif srcarr[i] == "-":
            tokenarray.append(["OPER", "SUB", ""])
        elif srcarr[i] == "(":
            notdefloop = 1
            while i+notdefloop < len(srcarr) and srcarr[i+notdefloop] != ")":
                notdefloop += 1
            tokenarray.append( [ "BLOCK", sourcelexer(''.join(srcarr[i+1:i+notdefloop])) ] )
            i += notdefloop-1
        elif srcarr[i] == ";":
            tokenarray.append(["PUNC", "SCOLON", ""])
        elif srcarr[i] == "=":
            tokenarray.append(["PUNC", "EQUAL", ""])
        elif srcarr[i:i+4] == ["C","A","L","L"]:
            notdefloop = 5
            while i+notdefloop < len(srcarr) and _isvalidvarchar( srcarr[i+notdefloop] ):
                notdefloop+=1
            functiontocall = srcarr[i+5:i+notdefloop]
            tokenarray.append( ["LOGIC", "CALL", ''.join(functiontocall) ])
            i += notdefloop-1
        elif srcarr[i:i+4] == ["L","O","A","D"]:
            notdefloop = 5
            tokenarray.append( ["LOGIC", "LOAD", ''.join(srcarr[i+5:i+notdefloop])])
            i += notdefloop-1
        elif srcarr[i] == "$":
            notdefloop = 1
            while i+notdefloop < len(srcarr) and _isvalidvarchar( srcarr[i+notdefloop] ):
                notdefloop += 1
            tokenarray.append( ["NAME", "VAR", ''.join(srcarr[i+1:i+notdefloop])] )
            i += notdefloop-1
        elif _ischarint( srcarr[i] ):
            notdefloop = 0
            while i+notdefloop < len(srcarr) and _ischarint( srcarr[i+notdefloop] ):
                notdefloop += 1
            tokenarray.append( ["LIT", "INT", ''.join(srcarr[i:i+notdefloop])] )
            i += notdefloop-1
        elif srcarr[i] == "\"":
            notdefloop = 1
            while i+notdefloop < len(srcarr) and srcarr[i+notdefloop] != "\"":
                notdefloop += 1  
            tokenarray.append( ["LIT", "STR", ''.join(srcarr[i+1:i+notdefloop])] )
            i += notdefloop
        i += 1
    return tokenarray

bytecode_arr = []

def virtualmachine(lexedsource):
    i = 0
    while i < len(lexedsource):
        if lexedsource[i][0] == "LIT":
            bytecode_arr.append("PUSH " + lexedsource[i][2])
        elif lexedsource[i][0] == "BLOCK":
            virtualmachine( lexedsource[i][1] )
        elif lexedsource[i][0] == "OPER":
            bytecode_arr.append("PUSH " + lexedsource[i+1][2])
            bytecode_arr.append(lexedsource[i][1])
        elif lexedsource[i][0] == "PUNC":
            if lexedsource[i][1] == "EQUAL":
                virtualmachine( lexedsource[i+1][1] )      
                bytecode_arr.append("SAVE " + lexedsource[i-1][2] )
                i+=1
        elif lexedsource[i][0] == "LOGIC":
            if lexedsource[i][1] == "CALL":
                virtualmachine( lexedsource[i+1][1] )
                bytecode_arr.append("CALL " + lexedsource[i][2])
                i+=1
        elif lexedsource[i][0] == "LOGIC":
            if lexedsource[i][1] == "LOAD":
                bytecode_arr.append("PUSH " + lexedsource[i+1][2])
                bytecode_arr.append("LOAD")
        i+=1

print("Input your code, type 'end' when you are done.")
sourcecode = ""
while True:
    currentline = input("code > ").upper()
    if currentline == "END":
        break
    sourcecode += currentline + ";"

lexedsource = sourcelexer(sourcecode)
print(lexedsource)
virtualmachine ( lexedsource )

print("")

print(bytecode_arr)

for cmd in bytecode_arr:
    _exec(cmd)
print("\nDEBUGGING :")
print("stack:", stack)
print("variables:", variables)